# README #

Welcome to ScalaSwingX, this scala library has a purpose make easy implement fast behavior using Java Swing in Scala Programming Language.

### What is this repository for? ###

* Using Java Swing Library in Scala fast, easy and functional way.

### How do I get set up? ###

* Only clone this repo using command line as follow: git clone https://bitbucket.org/slorenzot/scala-swingx
* No need extra configuration, import and use, just Scala Base and Java Swing Library included in JDK/JRE on JVM, of course
* No Database configuration for now

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Soulberto Lorenzo slorenzot at gmail.com or @slorenzot in Twitter