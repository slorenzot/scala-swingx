package scala.swingx.binding

import javax.swing.ButtonGroup
import javax.swing.event.{ChangeEvent, ChangeListener}

import scala.swingx.binding.contract.generic.Clickable

/**
  * Created by Soulberto Lorenzo on 7/28/2017.
  */
case class GroupCheckBoxBinding(components: List[javax.swing.JCheckBox]) extends Bindable {

  val group: ButtonGroup = new ButtonGroup

  components.foreach(c => {
    group.add(c)
    Binding.of(c)
  })

  def onChange(): GroupCheckBoxBinding = {
    this
  }

}
