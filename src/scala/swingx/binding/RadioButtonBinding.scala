package scala.swingx.binding

import javax.swing.AbstractButton

import scala.swingx.binding.contract.ToggleButton
import scala.swingx.binding.contract.generic.Clickable

/**
  * Created by Soulberto Lorenzo on 8/13/2017.
  */
case class RadioButtonBinding(swingComponent: javax.swing.JRadioButton) extends Bindable
  with Clickable[javax.swing.JRadioButton, RadioButtonBinding] {

  def select(isSelect: Boolean = true): RadioButtonBinding = {
    swingComponent.setSelected(isSelect)
    this
  }

  def onClick(action: () => Unit): RadioButtonBinding =  {
    super.onClick(swingComponent, action)
    this
  }

}
