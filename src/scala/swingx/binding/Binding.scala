package scala.swingx.binding

import javax.swing._

/**
  * Created by Soulberto Lorenzo on 7/27/2017.
  */
object Binding {

  def of(swingComponent: JProgressBar) = new ProgressBinding(swingComponent)

  def of(swingComponent: JSpinner) = new SpinnerBinding(swingComponent)

  def of(swingComponent: JSplitPane) = new SplitBinding(swingComponent)

  def of(swingComponent: JScrollPane) = new ScrollBinding(swingComponent)

  def of(swingComponent: JButton) = new ButtonBinding(swingComponent)

  def of(swingComponent: JLabel) = new LabelBinding(swingComponent)

  def of(swingComponent: JToggleButton) = new ToggleButtonBinding(swingComponent)

  def of(swingComponent: JCheckBox) = new CheckBoxBinding(swingComponent)

  def of(swingComponent: JRadioButton) = new RadioButtonBinding(swingComponent)

  def of(swingComponent: List[JRadioButton]) = new GroupRadioButtonBinding(swingComponent)

  def of(swingComponent: JMenuItem) = new MenuItemBinding(swingComponent)

  def of(swingComponent: JCheckBoxMenuItem) = new MenuCheckItemBinding(swingComponent)

  def of(swingComponent: JRadioButtonMenuItem) = new MenuRadioItemBinding(swingComponent)

  def of(swingComponent: JTextField) = new TextBinding(swingComponent)

  def of(swingComponent: JTextPane) = new TextBinding(swingComponent)

  def of(swingComponent: JTextArea) = new TextBinding(swingComponent)

  def of(swingComponent: JComboBox[String]) = new ComboBoxBinding(swingComponent)

  def of(swingComponent: JTable) = new TableBinding(swingComponent)

  def of(swingComponent: JSlider) = new SliderBinding(swingComponent)

}

trait Bindable
