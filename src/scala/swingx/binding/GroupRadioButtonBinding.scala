package scala.swingx.binding

import javax.swing.ButtonGroup
import javax.swing.event.{ChangeEvent, ChangeListener}

import scala.swingx.binding.contract.generic.Clickable

/**
  * Created by Soulberto Lorenzo on 8/13/2017.
  */
case class GroupRadioButtonBinding(components: List[javax.swing.JRadioButton]) extends Bindable {

  val group: ButtonGroup = new ButtonGroup

  components.foreach(c => {
    group.add(c)
    Binding.of(c)
  })

  def onChange(action: () => Unit): GroupRadioButtonBinding = {
    components.foreach(source => source.addChangeListener(new ChangeListener() {
      override def stateChanged(e: ChangeEvent) = action.apply()
    }))
    this
  }

}
