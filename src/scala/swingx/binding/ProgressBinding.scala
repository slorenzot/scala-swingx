package scala.swingx.binding

import javax.swing.event.{ChangeEvent, ChangeListener}

/**
  * Created by Soulberto Lorenzo on 8/11/2017.
  */
case class ProgressBinding(swingComponent: javax.swing.JProgressBar) extends Bindable {

  def onChange(action: (Int, Int) => Unit): ProgressBinding = {
    swingComponent.addChangeListener(new ChangeListener() {
      override def stateChanged(e: ChangeEvent) = action.apply(0, 0)
    })
    this
  }

}
