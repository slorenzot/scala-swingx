package scala.swingx.utils

import java.beans.PropertyChangeEvent
import java.util
import javax.swing.SwingWorker

import scala.swingx.Image
import scala.util.Random

/**
  * Created by Soulberto on 8/11/2017.
  */
case class ProgressMonitor[T <: java.awt.Window](title: String,
                                                 messages: Array[String],
                                                 cancelText: String = "Cancel",
                                                 onProcessing: (Int) => Unit,
                                                 onDone: () => Unit,
                                                 parent: T,
                                                 min: Int = 0,
                                                 max: Int = 100) {

  private val WARNING_ICON = "/resources/icons/Warning_48px.png"

  javax.swing.UIManager.put("ProgressMonitor.progressText", title)
  javax.swing.UIManager.put("OptionPane.informationIcon", Image.file(WARNING_ICON).toIcon)
  javax.swing.UIManager.put("OptionPane.cancelButtonText", cancelText)

  private val monitor = new javax.swing.ProgressMonitor(parent, messages, title, min, max)
  private val task = new Task()

  monitor.setMillisToDecideToPopup(0)
  monitor.setMillisToPopup(0)
  javax.swing.SwingUtilities.updateComponentTreeUI(parent)


  class Task extends SwingWorker[Int, Unit] {

    override def doInBackground() = {
      val random = new Random()
      var progress = 0
      monitor.setProgress(0)

      try {
        Thread.sleep(300)
        while (progress < 100 && !isCancelled()) {
          // Sleep for up to one second.
          Thread.sleep(random.nextInt(1000))
          // Make random progress.
          progress += random.nextInt(10)
          onProcessing(progress)

          monitor.setProgress(Math.min(progress, 100))
        }

        progress
      } finally progress
    }

    override def process(list: util.List[Unit]): Unit = {
      super.process(list)
    }

    override def done() = {
      monitor.setProgress(100)
      onDone()
    }
  }

  def start = task.execute()

}
